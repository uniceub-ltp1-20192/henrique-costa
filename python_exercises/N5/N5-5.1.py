#5.1

value = int(input("Informe um número qualquer:"))
if (value % 2)==0:
    print("Este número é par.")
else:
    print("Este número é ímpar.")

#5.2

num = int(input("Informe um número qualquer:"))

if num > 1:
    for i in range(2, num):
        if (num % i) == 0:
            print(num, "não é um número primo.")
            print(i, "vezes", num // i, "é", num)
            break
    else:
        print(num, "é um número primo.")

else:
    print(num, "não é um número primo.")

#5.3

val1 = float(input("Primeiro valor:"))
val2 = float(input("Segundo valor:"))
print(val1,"+",val2,"=", val1+val2,"\n",val1,"-",val2,"=", val1-val2,"\n",val1,"*",val2,"=", val1*val2,"\n",val1,"/",val2,"=", val1/val2,)

#5.4

age= int(input("Qual a sua idade?"))
semester= int(input("Está em qual semestre da faculdade?"))
curseLength= int(input("Qual a duração do seu curso?(em semestres)"))
print ("Voce ira se formar com", ((curseLength-semester)/2)+age,"anos.")

#5.5
age= int(input("Qual a sua idade?"))
semester= int(input("Está em qual semestre da faculdade?"))
curseLength= int(input("Qual a duração do seu curso?(em semestres)"))
late = int(input("Quantos semestres voce esta atrasado?"))
print ("Voce ira se formar em", ((curseLength-semester+late)/2),"anos.")


