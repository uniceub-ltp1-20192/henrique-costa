#6.1
friends = ['Matheus','Jonas','Nicolas','Shiva']
print(friends[0],"\n",friends[1],"\n",friends[2],"\n",friends[3])

#6.2

friends = ['Matheus','Jonas','Nicolas','Shiva']
for i in friends:
    print("Seja bem vindo,",i)

#6.3

cars=['Ferrari','Porsche','Lamborgini','Bentley']
messages=['Eu sonho em ter um carro da','Meu carro preferido é da','Meu pai tem um carro da','Não gosto de']
for i in cars:
    for l in messages:
        print(l,i)
