from Entidades.gato import Gato

class Siames(Gato):
  def __init__(self,corDosOlhos = "azul"):
    super().__init__()
    self._corDosOlhos = corDosOlhos

  @property
  def corDosOlhos(self):
    return self._corDosOlhos

  @corDosOlhos.setter
  def corDosOlhos(self,corDosOlhos):
    self._corDosOlhos = corDosOlhos

  def miar(self):
    print("Miau...Miauu")
  
  def __str__(self):
    return '''
    ---- Siamês ---- 
    Identificador: {}
    Tamanho: {}
    Raca : {}
    Cor : {}
    Peso: {}
    Cor do olho: {}
    '''.format(self.identificador,self.tamanho
    ,self.raca,self.cor,self.peso,self.corDosOlhos)