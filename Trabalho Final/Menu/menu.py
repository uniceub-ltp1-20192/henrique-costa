from Validador.validador import Validador
from Dados.dados import Dados
from Entidades.siames import Siames

class Menu:
 
    @staticmethod
    def menuPrincipal():
        print("""
0 - Sair
1 - Consultar
2 - Inserir
3 - Alterar
4 - Deletar
""")
        return Validador.validarOpcaoMenu("[0-4]")
    @staticmethod
    def menuConsultar():
        print("""
0 - Voltar
1 - Consultar por Identificador
2 - Consultar por Propriedade
""")
        return Validador.validarOpcaoMenu("[0-2]")

    @staticmethod
    def iniciarMenu():
        d = Dados()
        opMenu = ""
        while opMenu != "0":
            opMenu = Menu.menuPrincipal()
            if opMenu == "1":
                print("entrou em Consultar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if(retorno != None):
                            print(retorno)   
                        else:
                            print("""
                            Nao foi encontrado 
                            nenhum registro com
                            esse identificador
                            """)                
                      
                    elif opMenu == "2":
                        retorno = Menu.menuBuscaPorAtributo(d)
                        if(retorno != None):
                            print(retorno)   
                        else:
                            print("""
                            Nao foi encontrado 
                            nenhum registro com
                            esse atributo
                            """) 
                       
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
               
            elif opMenu == "2":
                print("entrou em Inserir")
                Menu.menuInserir(d)

            elif opMenu == "3":
                print("entrou em alterar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno,d)
                    elif opMenu == "2":
                        retorno = Menu.menuBuscaPorAtributo(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno,d)    
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
            elif opMenu == "4":
                print("entrou em deletar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuDeletar(retorno,d)
                    elif opMenu == "2":
                        retorno = Menu.menuBuscaPorAtributo(d)
                        if retorno != None:
                            Menu.menuDeletar(retorno,d)    
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
            elif opMenu == "0":
                print("Saindo")
            elif opMenu == "":
                print("")
            else:
                print("Informe uma opcao valida")

    @staticmethod
    def menuBuscaPorIdentificador(d :Dados):
        retorno = d.buscarPorIdentificador(Validador.verificarInteiro())
        return retorno

    @staticmethod
    def menuBuscaPorAtributo(d :Dados):
        retorno = d.buscarPorAtributo(input("Informe um tamanho para busca:"))
        return retorno


    @staticmethod
    def menuInserir(d):
        p = Siames()
        p.peso = Validador.validarPeso(p.peso, "Informe um peso: ")
        p.tamanho = Validador.validarTamanho(p.tamanho, "Informe o tamanho: ")
        p.raca =  Validador.validarRaca(p.raca, "Informe uma raça: ")
        p.cor = Validador.validarCor(p.cor, "Informe cor: ")
        p.corDosOlhos = Validador.validarCorDosOlhos(p.corDosOlhos, "Informe a cor dos olhos: ")
        d.inserir(p)
    
    @staticmethod
    def menuAlterar(retorno,d):
        retorno.peso = Validador.validarPeso(retorno.peso,"Informe um peso: ")
        retorno.tamanho = Validador.validarTamanho(retorno.tamanho,"Informe o tamanho :") 
        retorno.raca = Validador.validarRaca(retorno.raca,"Informe a raça: ")  
        retorno.cor = Validador.validarCor(retorno.cor,"Informe a cor: ") 
        retorno.corDosOlhos = Validador.validarCorDosOlhos(retorno.corDosOlhos,"Informe a cor dos olhos: ")
        d.alterar(retorno)
    
    @staticmethod
    def menuDeletar(retorno,d):
        print(retorno)
        deletar = input("""
        Deseja deletar o registro:
        S - Sim
        """)
        if deletar == "S" or deletar == "s":
            d.deletar(retorno)
            print("""
            Registro deletado
            """)
        else:
            print("""
            Registro nao foi deletado
            """)
