import re

class Validador:

    @staticmethod
    def verificarInteiro():
        teste = False
        while teste == False:
            valor = input("Informe um inteiro:")
            v  = re.match("\d+",valor)
            if v != None:
                teste = True
        return int(valor)

    @staticmethod
    def validarOpcaoMenu(expReg):
        teste = False
        while teste == False:
            opcao = input("Informe uma opcao:")
            v  = re.match(expReg,opcao)
            if v != None:
                return opcao
            else:
                print("Opcao invalida! Infome um valor entre {} "
                .format(expReg))
  
    @staticmethod
    def validarPeso(ppeso, msg):
        tsx = False

        while tsx == False:
            novoPeso = input(msg)
            if novoPeso == None or novoPeso == '':
                return ppeso
            if int(novoPeso) >= int(4) and int(novoPeso) <= int(8):
                return novoPeso
            else:
                print("Inválido, um siamês pesa ente 2 a 8 Kg")

    
    @staticmethod
    def validarTamanho(ptamanho, msg):
        tsx = False
        
        while tsx == False:
            novoTamanho = input(msg)
            if novoTamanho == None or novoTamanho == '':
                return ptamanho
            if int(novoTamanho) >= int(15) and int(novoTamanho) <= int(20):
                return novoTamanho
            else:
                print("Inválido, um siamês mede de 15 a 20cm")


    @staticmethod
    def validarCor(pcor, msg):
        test = False
        a = 'Preto'
        b = 'Branco'
        c = 'Creme'
        d = 'Bege'
        e = 'Marrom'

        while test == False:
            novaCor = input(msg)
            if novaCor == None or novaCor == '':
                return pcor
            if novaCor == a or novaCor == b or novaCor == c or novaCor == d or novaCor == e:
                return novaCor
            else:
                print("Cores válidas: Preto / Branco / Creme / Bege / Marrom")
    
    @staticmethod
    def validarRaca(praca, msg):
        test = False
        a = 'Siames'
        b = 'siames'

        while test == False:
            novaRaca = input(msg)
            if novaRaca == None or novaRaca == '':
                return praca
            if novaRaca == a or novaRaca == b:
                return novaRaca
            else:
                print("Raças válidas: (Siames) ")

    @staticmethod
    def validarCorDosOlhos(pcorolho, msg):
        test = False
        a = 'Azul'
        b = 'azul'

        while test == False:
            novaCorDosOlhos = input(msg)
            if novaCorDosOlhos == None or novaCorDosOlhos == '':
                return pcorolho
            if novaCorDosOlhos == a or novaCorDosOlhos == b :
                return novaCorDosOlhos
            else:
                print("Cores válidas: (Azul)")
    


    
